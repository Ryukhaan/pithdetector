# Pith Dectection

An algorithm to detect the pith location on untreated wood cross-section images.


## Requirements

1. OpenCV 3.4.3
2. OpenMP

## How to install

### 1. Using Cmake

Cmake is not available at the moment.

### 2. Using XCode

Open the project with xcodeproject file
Then run. (beware of OpenCV and OpenMP search path)


## Run Your Own Dataset

All loading/saving paths are written in **filepath.json**

Base field is the basepath for searching (both loading and saving) the resources (images, parameters, ...).

#### How it works ? 

The directory to load images is :
> JSON["base"]+JSON["load"]["images"]+JSON["dataset"]

The directory to load parameters is :
> JSON["base"]+JSON["load"]["parameters"]+JSON["dataset"]

The file in which piths location are saved is : 
> JSON["base"]+JSON["save"]["piths"]+JSON["dataset"]+JSON["name"]

(See **main.cpp**)
