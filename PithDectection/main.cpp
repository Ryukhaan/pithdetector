//
//  main.cpp
//  PithDectection
//
//  Created by Remi DECELLE on 23/12/2018.
//  Copyright © 2018 Remi DECELLE. All rights reserved.
//


// Basics headers for I/O manipulation and basics structures (string, ...)
#include <iostream>
#include <stdio.h>
#include <string>
#include <fstream>
#include <sstream>

// OpenCV headers for image processing
#include <opencv2/core.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/plot.hpp>
#include <opencv2/highgui.hpp>

// OpenMP headers for parallelism
#include <omp.h>

// My own headers
#include "timer.hpp"
#include "ui.hpp"
#include "math.hpp"
#include "factory.hpp"

#include "LegacyModel.h"
#include "AdjacentModel.hpp"
#include "IHough.hpp"

#include "DoGTreater.h"
#include "LoGTreater.h"

bool debug = false;
bool writer = true;

using namespace cv;
using namespace std;

static const string BBF = "BBF";

int main( int argc, char** argv )
{
    string load_paths = "/Users/remidecelle/Documents/TreeTrace/PithDectection/PithDectection/filepath.json";
    FileStorage all_paths(load_paths, FileStorage::READ);
    
    string dataset = String(all_paths["dataset"]);
    string file_name = String(all_paths["name"]);
    
    string param_loading    = String(all_paths["base"]) + String(all_paths["load"]["parameters"]) + dataset + ".json";
    string image_loading    = String(all_paths["base"]) + String(all_paths["load"]["images"]) + dataset + "/";
    string truths_loading   = String(all_paths["base"]) + String(all_paths["load"]["truths"]) + dataset + ".csv";
    string piths_loading    = String(all_paths["base"]) + String(all_paths["load"]["piths"]) + dataset + ".csv";
    string param_saving     = String(all_paths["base"]) + String(all_paths["save"]["parameters"]) + dataset + "/" + file_name + ".json";
    string image_saving     = String(all_paths["base"]) + String(all_paths["save"]["images"]) + dataset + "/";
    string piths_saving     = String(all_paths["base"]) + String(all_paths["save"]["piths"]) + dataset + "/" + file_name + ".csv";
    
    cout << param_loading << endl;
    FileStorage parameters(param_loading, FileStorage::READ);
    
    ModelFactory* my_factory = new ModelFactory();
    LegacyModel* model = (LegacyModel*) my_factory->create(factory::LegacyModelType, param_loading);
    IHough* hough_model = (IHough*) my_factory->create(factory::IHoughType, param_loading);
    
    string names[200];
    vector<double> mycsv;
    mycsv = ui::read_csv(truths_loading, names);
    
    // Save all pith estimation, file's name and time computation
    vector<double> all_values;
    
    // Iterate through files and save position
    int file_number = 0;
    int px, py;
    
    // Variables for computing error (euclidean distance)
    double ps, vx, vy;
    double err = 0, delta = 0;
    
    // Optimization's variables
    file_number = 0;
    err = 0;
    delta = 0;
    all_values.clear();
    bool dont_count = false;
    while ( names[file_number] != "" )
    {
        // Read image from file (path)
        string image_name = names[file_number];
        string path = image_loading + image_name;
        Mat image, scaled_image;
        
        image = imread(path, CV_LOAD_IMAGE_COLOR);
        if(! image.data )
        {
            cout <<  "Could not open or find the image" << std::endl ;
            return -1;
        }
        
        Point pith_estimation;
        timer::tic();
        // Agents-based Method : Step One
        model->set_dimensions(image);
        model->reinit_agents();
        pith_estimation = model->execute(image);
        
        // State-of-the-art Method : Step Two
        hough_model->set_starter(pith_estimation);
        pith_estimation = hough_model->estimate(image);
        
        px = pith_estimation.x;
        py = pith_estimation.y;
        
        ps = mycsv.at(3*file_number+0);
        vx = mycsv.at(3*file_number+1);
        vy = mycsv.at(3*file_number+2);

        delta = (ps * sqrt(pow(vx - px, 2.0) + pow(vy - py, 2.0)));
        cout << "\t" << "Error: " << delta << endl;
        err += delta;
        
        // Save image
        Mat cloner = image.clone();
        drawMarker(cloner, Point(px, py), Scalar(50,50,255), MARKER_CROSS, 128, 2);
        string image_path = image_saving + image_name;
        
        imwrite(image_path, cloner);
        
        // Pushes all values (ground truths, pit estimation, time...)
        all_values.push_back(file_number);
        all_values.push_back(ps);
        all_values.push_back(vx);
        all_values.push_back(vy);
        all_values.push_back(px);
        all_values.push_back(py);
        all_values.push_back(delta);
        all_values.push_back(timer::get_toc());
        
        file_number++;
    }
    // Save Datas
    
    ofstream outputCSV(piths_saving);
    Mat datas = Mat(all_values);
    datas = datas.reshape(0, file_number);
    outputCSV << format(datas, Formatter::FMT_NUMPY) << endl;
    outputCSV.close();
    
    // Save Parameters
    FileStorage saving(param_saving, FileStorage::WRITE);
    FileNode root = parameters["SMA"];
    FileNodeIterator it = root.begin(), it_end = root.end();
    for (; it != it_end; ++it)
    {
        FileNode nodes = *it;
        FileNodeIterator nit = nodes.begin(), nit_end = nodes.end();
        for (; nit != nit_end; ++nit)
        {
            FileNode current = *nit;
            saving << current.name() << (double)*nit;
        }
    }
    return 0;
}
