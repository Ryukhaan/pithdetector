//
//  visitor.hpp
//  PithDectection
//
//  Created by Remi DECELLE on 09/04/2019.
//  Copyright © 2019 Remi DECELLE. All rights reserved.
//

#ifndef visitor_hpp
#define visitor_hpp

#include <stdio.h>
#include "LegacyModel.h"
#include "AdjacentModel.hpp"

/**
 * Visitor Class
 */
class Visitor
{
public:
    void visit(LegacyModel* model);
    void visit(AdjacentModel* model);
};

#endif /* visitor_hpp */
