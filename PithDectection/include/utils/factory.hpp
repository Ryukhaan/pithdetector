//
//  factory.hpp
//  PithDectection
//
//  Created by Remi DECELLE on 11/04/2019.
//  Copyright © 2019 Remi DECELLE. All rights reserved.
//

#ifndef factory_hpp
#define factory_hpp

#include <stdio.h>
#include <string>
#include "AbstractModel.h"

namespace factory {
    typedef enum model_enum
    {
        LegacyModelType,
        AdjacentModelType,
        IHoughType
    } model_type;
}

class ModelFactory
{
public:
    ModelFactory() {};
    ~ModelFactory() {};
    
    AbstractModel* create(factory::model_type type, std::string loading_path);
};


#endif /* factory_hpp */
