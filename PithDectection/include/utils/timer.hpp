//
//  timer.hpp
//  PithDectection
//
//  Created by Remi DECELLE on 18/01/2019.
//  Copyright © 2019 Remi DECELLE. All rights reserved.
//

#ifndef timer_hpp
#define timer_hpp

namespace timer {
    
    extern double tt_tic;
    extern double tt_toc;
    
    /**
     * Initialize the timer
     */
    void tic();

    /**
     * Save elapsed time from tic to toc
     */
    void toc();

    /**
     * @return Elapsed time
     */
    double get_toc();

    /**
     * @return Time when timer was initialized
     */
    double get_tic();
}


#endif /* timer_hpp */
