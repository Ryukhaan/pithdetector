//
//  math.hpp
//  PithDectection
//
//  Created by Remi DECELLE on 18/01/2019.
//  Copyright © 2019 Remi DECELLE. All rights reserved.
//

#ifndef math_hpp
#define math_hpp

#include <opencv2/core.hpp>
#include <opencv2/imgproc.hpp>

using namespace cv;

namespace math {
    

    /**
     * Defines the kernel size according to the standard deviation
     * Size = 2 * ceil( 2 * sigma ) + 1
     * 
     * @param sigma Standard deviation
     * @return Size of the kernel (width, height)
     */
    inline Size gaussian_kernel(double sigma);
    
    /**
     * Equivalent linspace function in Matlab
     * Given an interval [a, b] and step n, create the values : a, a+h, ... b-h, b
     * 
     * @param startP Starting point (a)
     * @param Endp Ending point (b)
     * @param interval Length of the linspace
     * @return Vector containing all values
     */
    Mat linspace(double startP, double Endp, int interval);
    
    /**
     * Compute difference of two gaussian filters with different standard deviation
     * 
     * @param input Original image
     * @param sigma_high The highest standard deviation
     * @param sigma_low The lowest standard deviation
     * @return Filtered image
     */
    Mat difference_of_gaussian(Mat input, double sigma_high, double sigma_low);

    /**
     * Compute a Laplacian of Gaussian
     * 
     * @param input Original image
     * @param sigma Standard deviation of the gaussian filter
     * @param kernel_size Kernel size of gaussian filter
     * @return Filtered image
     */
    Mat laplacian_of_gaussian(Mat input, double sigma, int kernel_size);
    
    /**
     * Compute fft2 (see Matlab)
     * 
     * @param input Original image
     * @return FFT
     */
    Mat fft2(Mat input);

    /**
     * Compute Triangle Method for threshold an image (see Zack et al. 1977)
     * 
     * @param image Original image
     * @return Threshold image
     */
    double triangle_thresh(Mat image);
}

#endif /* math_hpp */
