//
//  ui.hpp
//  PithDectection
//
//  Created by Remi DECELLE on 18/01/2019.
//  Copyright © 2019 Remi DECELLE. All rights reserved.
//

#ifndef ui_hpp
#define ui_hpp

#include <opencv2/core.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/plot.hpp>
#include <opencv2/highgui.hpp>
#include <iostream>
#include <stdio.h>

namespace ui
{
    
    extern double scale_imagesc;
    /**
     * Equivalent to imagesc in Matlab (with colormap gray)
     * Set waiting to 0 in order to wait for a key to be pressed
     * 
     * @param input Image to display
     * @param waiting Waiting time (in ms) before closing window (0 for waiting an user action)
     */
    void imagesc(cv::Mat input, int waiting);

    /**
     * Read csv file and retrieve images' name
     * 
     * @param path File's path
     * @param input Array of images' name
     * @return Vector with pith location and pixel resolution (p, x, y)
     */
    std::vector<double> read_csv(std::string path,  std::string* input);

    /**
     * Read only pith position
     *
     * @param path Path to the csv file
     * @return Vector with pith location and pixel resolution (p, x, y)
     */
    std::vector<double> read_pith(std::string path);

    /**
     * Save the input image into a png file
     * 
     * @param input Image to be saved
     * @param filename Image's name (without extension)
     */
    void saveas(cv::Mat input, std::string filename);
}

#endif /* ui_hpp */
