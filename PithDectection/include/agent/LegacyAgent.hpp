//
//  LegacyAgent.hpp
//  PithDectection
//
//  Created by Remi DECELLE on 18/01/2019.
//  Copyright © 2019 Remi DECELLE. All rights reserved.
//

#ifndef LegacyAgent_hpp
#define LegacyAgent_hpp

//#include <omp.h>

#include "IAgent.h"
#include "Maximum.h"
#include "LineIntersecter.h"
#include "FFTOrienter.h"
#include "HOGOrienter.h"
#include "TriangleThresher.h"

#include "timer.hpp"

class LegacyAgent : public IAgent
{
private:
    double pas;
    double epsilon;
    double sigma_vote;
    
    int padding;
    int H, W;
    
public:
    
    LegacyAgent(int x,
                int y,
                bool stopped,
                IThresher* thresher,
                IOrienter* orienter,
                IIntersecter* intersecter,
                IBarycenter* barycenter,
                double pas,
                double epsilon,
                double sigma,
                int padding,
                int H,
                int W);
    
    void update_pas(double initial_value, int t, int N)
    {
        pas = initial_value * ( 1.0 - static_cast<double>(t) / N);
        //double C = static_cast<double>(N);
        //pas = C * initial_value / (t + C);
    }
    
    Rect getNeighborhood()
    {
        int dx = x - padding;
        int dy = y - padding;
        dx = min(static_cast<double>(W), max(0.0, static_cast<double>(dx)));
        dy = min(static_cast<double>(H), max(0.0, static_cast<double>(dy)));
        return Rect(dx, dy, 2*padding, 2*padding);
    }
    
    void move(Mat image)
    {
        Rect clip = getNeighborhood();
        Mat neighbor = image(clip);
        
        //ui::imagesc(neighbor, 0);
        //Mat bwneighbor = thresher->threshhold(neighbor);
        //if (neighbor.allocator != NULL) {
        Mat bwneighbor = neighbor.clone();
        
        vector<double> orientations = orienter->find_orientations(bwneighbor);
        
        Mat intersection = intersecter->intersect(orientations, x-padding, y-padding, image);
        //ui::imagesc(intersection, 0);
        
        Point new_position = barycenter->optimum(intersection);
        
        //Point old_position = this->getPosition();
        if (new_position.x == -1 && new_position.y == -1)
        {
            is_stopped = true;
            return;
        }
        update(new_position);
        //}
        /*
        Mat display = image.clone();
        cvtColor(display, display, CV_GRAY2BGR);
        drawMarker(display, new_position, Scalar(50,255,127), MARKER_CROSS, 32, 2);
        drawMarker(display, this->getPosition(), Scalar(50,127,255), MARKER_CROSS, 32, 2);
        drawMarker(display, old_position, Scalar(255,127,50), MARKER_CROSS, 32, 2);
        ui::imagesc(display, 0);
        */
        
    };
    
    void move(Mat norm, Mat direction) {};
    
    Mat vote();
    void update(Point p);
};

#endif /* LegacyAgent_hpp */
