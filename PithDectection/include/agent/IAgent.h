//
//  IAgent.h
//  PithDectection
//
//  Created by Remi DECELLE on 18/01/2019.
//  Copyright © 2019 Remi DECELLE. All rights reserved.
//

#ifndef IAgent_h
#define IAgent_h

#include <opencv2/core.hpp>
#include <opencv2/imgproc.hpp>

#include <vector>
#include <algorithm>

#include "math.hpp"
#include "ui.hpp"
#include "IThresher.h"
#include "IOrienter.h"
#include "IIntersecter.h"
#include "IBarycenter.h"

using namespace cv;
using namespace std;

class IAgent
{
protected:
    int x;
    int y;

    IThresher* thresher;
    IOrienter* orienter;
    IIntersecter* intersecter;
    IBarycenter* barycenter;
    
    bool is_stopped;
    
public:
    
    IAgent(int x,
           int y,
           bool stopped,
           IThresher* thresher,
           IOrienter* orienter,
           IIntersecter* intersecter,
           IBarycenter* barycenter)
    {
        this->x = x;
        this->y = y;
        this->is_stopped  = stopped;
        this->thresher    = thresher;
        this->orienter    = orienter;
        this->intersecter = intersecter;
        this->barycenter  = barycenter;
    };
    
    Point getPosition()
    {
        return Point(this->x, this->y);
    }
    
    bool is_movable()
    {
        return !is_stopped;
    }
    
    virtual void update_pas(double alpha, int t, int N) = 0;
    virtual Mat vote() = 0;
    virtual void update(Point p) = 0;
    virtual Rect getNeighborhood() = 0;
    
    virtual void move(Mat image)
    {
        Rect clip = getNeighborhood();
        Mat neighbor = image(clip);
        
        //Mat bwneighbor = thresher->threshhold(neighbor);
        Mat bwneighbor = neighbor.clone();
        
        vector<double> orientations = orienter->find_orientations(bwneighbor);
        
        Mat intersection = intersecter->intersect(orientations, x, y, image);
        
        Point new_position = barycenter->optimum(intersection);
        
        update(new_position);
    };
    
    virtual void move(Mat norm, Mat direction) = 0;
};

#endif /* IAgent_h */
