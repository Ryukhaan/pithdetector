//
//  AdjacentAgent.hpp
//  PithDectection
//
//  Created by Remi DECELLE on 09/04/2019.
//  Copyright © 2019 Remi DECELLE. All rights reserved.
//

#ifndef AdjacentAgent_hpp
#define AdjacentAgent_hpp

#include <stdio.h>

#include "IAgent.h"
#include "Maximum.h"
#include "LineIntersecter.h"
#include "FFTOrienter.h"
#include "HOGOrienter.h"
#include "TriangleThresher.h"

#include "timer.hpp"

class AdjacentAgent : public IAgent
{
private:
    double pas;
    double epsilon;
    double sigma_vote;
    
    int padding;
    int H, W;
    
public:
    
    AdjacentAgent(int x,
                int y,
                bool stopped,
                IThresher* thresher,
                IOrienter* orienter,
                IIntersecter* intersecter,
                IBarycenter* barycenter,
                double pas,
                double epsilon,
                double sigma,
                int padding,
                int H,
                int W);
    
    void update_pas(double initial_value, int t, int N)
    {
        pas = initial_value * ( 1.0 - static_cast<double>(t) / N);
        //pas = 1.0;
    }
    
    Rect getNeighborhood()
    {
        int dx = x - padding;
        int dy = y - padding;
        dx = min(static_cast<double>(W), max(0.0, static_cast<double>(dx)));
        dy = min(static_cast<double>(H), max(0.0, static_cast<double>(dy)));
        return Rect(dx, dy, 2*padding, 2*padding);
    }
    
    void move(Mat image)
    {
        Rect clip = getNeighborhood();
        Mat neighbor = image(clip);
        
        Mat bwneighbor = neighbor.clone();
        
        vector<double> orientations = orienter->find_orientations(bwneighbor);
        
        Mat intersection = intersecter->intersect(orientations, x-padding, y-padding, image);
        
        Point new_position = barycenter->optimum(intersection);
        
        if (new_position.x == -1 && new_position.y == -1)
        {
            is_stopped = true;
            return;
        }
        update(new_position);
        
    };
    
    void move(Mat norm, Mat direction) {};
    
    Mat vote();
    void update(Point p);
    void move_to(Point target, double gamma);
    
    double get_pas()
    {
        return pas;
    };
};

#endif /* LegacyAgent_hpp */
