//
//  IModel.h
//  PithDectection
//
//  Created by Remi DECELLE on 18/01/2019.
//  Copyright © 2019 Remi DECELLE. All rights reserved.
//

#ifndef IModel_h
#define IModel_h

#include <stdio.h>
#include <vector>

#include <omp.h>

#include "ui.hpp"
#include "timer.hpp"
#include "ITreater.h"
#include "IAgent.h"
#include "AbstractModel.h"

using namespace std;

class IModel : public AbstractModel
{
protected:
    int __num_max_iter;
    int __num_agents;
    int __window_height;
    int __window_width;
    
    ITreater* __treater;
    vector<IAgent*> agents;
    
public:
    
    IModel() {};
    
    IModel(ITreater* treater, int height, int width, int grid_size, int nmi) :
        __treater(treater),
        __num_agents(grid_size*grid_size),
        __num_max_iter(nmi),
        __window_height(height), __window_width(width) {};


    void view_agents(Mat image, int wait);
    void save_agents(Mat image, string filename);
    
    virtual Point execute(Mat image) = 0;
    virtual void reinit_agents() = 0;
    virtual void load(string parameters_path) = 0;
    virtual void accept(class Visitor &v) = 0;
    
    void set_dimensions(int height, int width)
    {
        __window_height = height;
        __window_width  = width;
    };
    
    void set_dimensions(Mat image)
    {
        __window_height = image.rows;
        __window_width  = image.cols;
    };
    
    void set_number_of_agents(int noa)
    {
        __num_agents = noa;
    };
    
    void set_num_max_iter(int num_max_iter)
    {
        __num_max_iter = num_max_iter;
    };
    
    void set_treater_scale(double scale)
    {
        __treater->set_scale(scale);
    };

    int get_number_of_agents()
    {
        return __num_agents;
    };
    
    int get_num_max_iter()
    {
        return __num_max_iter;
    };

    double get_treater_scale()
    {
        return __treater->get_scale();
    };


    
};

#endif /* IModel_h */
