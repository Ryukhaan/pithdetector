//
//  AbstractModel.h
//  PithDectection
//
//  Created by Remi DECELLE on 11/04/2019.
//  Copyright © 2019 Remi DECELLE. All rights reserved.
//

#ifndef AbstractModel_h
#define AbstractModel_h

class AbstractModel
{
public:
    AbstractModel() {};
    ~AbstractModel() {};
};

#endif /* AbstractModel_h */
