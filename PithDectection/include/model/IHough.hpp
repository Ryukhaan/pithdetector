//
//  IHough.hpp
//  PithDectection
//
//  Created by Remi DECELLE on 31/01/2019.
//  Copyright © 2019 Remi DECELLE. All rights reserved.
//

#ifndef IHough_hpp
#define IHough_hpp

#include <stdio.h>
#include <opencv2/core.hpp>
#include <opencv2/imgproc.hpp>
#include "AbstractModel.h"
#include "ITreater.h"
#include "IBarycenter.h"

using namespace std;

/**
 * Abstract Class for create Hough-based method for pith detection
 */
class IHough : public AbstractModel
{
private:
    int __window_size;
    int __median_filter_size;
    int __max_iter;
    double __epsilon;
    double __ratio;
    
    Point __starter;
    ITreater* __treater;
    
public:
    IHough() {};
    IHough(string load_path);
    ~IHough() {};
    
    void load(string parameters_path);
    Point estimate(Mat image);

    /**
     * Define all setters
     */
    void set_starter(Point point)
    {
        __starter = point;
    };
    
    void set_window_size(int ws)
    {
        __window_size = ws;
    };
    
    void set_median_filter_size(int mfs)
    {
        __median_filter_size = mfs;
    };
    
    void set_ratio(double ratio)
    {
        __ratio = ratio;
    };

    void set_max_iter(int max_iter)
    {
        __max_iter = max_iter;
    };

    void set_epsilon(double eps)
    {
        __epsilon = eps;
    };

    /**
     * Define all getters
     */
    
    int get_window_size()
    {
        return __window_size;
    };

    int get_median_filter_size()
    {
        return __median_filter_size;
    };

    int get_max_iter()
    {
        return __max_iter;
    };

    double get_epsilon()
    {
        return __epsilon;
    };

    double get_ratio()
    {
        return __ratio;
    };

};
#endif /* IHough_hpp */
