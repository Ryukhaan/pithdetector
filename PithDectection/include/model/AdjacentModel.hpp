//
//  AdjacentModel.hpp
//  PithDectection
//
//  Created by Remi DECELLE on 09/04/2019.
//  Copyright © 2019 Remi DECELLE. All rights reserved.
//

#ifndef AdjacentModel_hpp
#define AdjacentModel_hpp

#include <stdio.h>
#include <string>

#include "AdjacentAgent.hpp"
#include "LoGTreater.h"
#include "EnergyTreater.hpp"
#include "PeerLineIntersecter.hpp"
#include "IModel.h"


class AdjacentModel : public IModel
{
private:
    int __fft_window_size;
    int __nbins;
    int __quantizer;
    double __pas;
    double __epsilon;
    double __sigma;
    
public:
    AdjacentModel(ITreater* treater,
                  int grid_size,
                  int nmi,
                  int window_height,
                  int window_width,
                  int fft_window_size,
                  int nbins,
                  double pas,
                  double epsilon,
                  double sigma_vote);
    
    AdjacentModel(string load_path);
    
    Point execute(Mat image);
    void reinit_agents();
    void load(string parameters_path);
    void accept(Visitor &v);
    
    void set_bins(int bins)
    {
        __nbins = bins;
    };
    
    void set_sub_window(int sub_window)
    {
        __fft_window_size = sub_window;
    };
    
    void set_sigma(double sigma)
    {
        __sigma = sigma;
    };
    
    void set_gamma(double gamma)
    {
        __pas = gamma;
    };
    
    void set_quantizer(int quantizer)
    {
        __quantizer = quantizer;
    };
    
    void set_epsilon(double epsilon)
    {
        __epsilon = epsilon;
    };
    
    void set_treater_sigma(double sigma)
    {
        ((LoGTreater*) __treater)->set_sigma(sigma);
    };
    
    void set_treater_kernel(int kernel_size)
    {
        ((LoGTreater*) __treater)->set_kernel_size(kernel_size);
    };
    
    
    int get_bins()
    {
        return __nbins;
    };
    
    int get_sub_window()
    {
        return __fft_window_size;
    };
    
    double get_sigma()
    {
        return __sigma;
    };
    
    double get_gamma()
    {
        return __pas;
    };
    
    int get_quantizer()
    {
        return __quantizer;
    };
    
    double get_epsilon()
    {
        return __epsilon;
    };
    
    double get_treater_sigma()
    {
        return ((LoGTreater*) __treater)->get_sigma();
    };
    
    int get_treater_kernel()
    {
        return ((LoGTreater*) __treater)->get_kernel_size();
    };
    
};

#endif /* AdjacentModel_hpp */
