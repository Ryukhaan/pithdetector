//
//  LineIntersecter.h
//  PithDectection
//
//  Created by Remi DECELLE on 18/01/2019.
//  Copyright © 2019 Remi DECELLE. All rights reserved.
//

#ifndef LineIntersecter_h
#define LineIntersecter_h

#include <opencv2/core.hpp>
#include <opencv2/imgproc.hpp>

#include "IIntersecter.h"

using namespace cv;
using namespace std;

class LineIntersecter : public IIntersecter
{
private:
    int __sub_window_size;
    
public:
    LineIntersecter(int swsize) : __sub_window_size(swsize) {};
    
    Mat intersect(vector<double> orientations, int dx, int dy, Mat image);
};

#endif /* LineIntersecter_h */
