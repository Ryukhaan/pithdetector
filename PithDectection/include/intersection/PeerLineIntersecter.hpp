//
//  PeerLineIntersecter.hpp
//  PithDectection
//
//  Created by Remi DECELLE on 06/02/2019.
//  Copyright © 2019 Remi DECELLE. All rights reserved.
//

#ifndef PeerLineIntersecter_hpp
#define PeerLineIntersecter_hpp

#include <stdio.h>
#include <opencv2/core.hpp>
#include <opencv2/imgproc.hpp>

#include "IIntersecter.h"

using namespace cv;
using namespace std;

class PeerLineIntersecter : public IIntersecter
{
private:
    int __sub_window_size;
    int __quantizer;
    
public:
    PeerLineIntersecter(int swsize) : __sub_window_size(swsize), __quantizer(8) {};
    PeerLineIntersecter(int swsize, int quantizer) : __sub_window_size(swsize), __quantizer(quantizer) {};
    
    Mat intersect(vector<double> orientations, int dx, int dy, Mat image);
    
};

#endif /* PeerLineIntersecter_hpp */
