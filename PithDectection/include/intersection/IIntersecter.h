//
//  IIntersecter.h
//  PithDectection
//
//  Created by Remi DECELLE on 18/01/2019.
//  Copyright © 2019 Remi DECELLE. All rights reserved.
//

#ifndef IIntersecter_h
#define IIntersecter_h

#include <opencv2/core.hpp>
#include <opencv2/imgproc.hpp>
using namespace cv;
using namespace std;

/**
 * Implements your own method for creating accumulation space
 */
class IIntersecter {
    
public:
    IIntersecter() {};
    ~IIntersecter() {};
    
    virtual Mat intersect(vector<double> orientations, int dx, int dy, Mat image) = 0;
};
#endif /* IIntersecter_h */
