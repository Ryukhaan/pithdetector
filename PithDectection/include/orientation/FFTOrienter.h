//
//  FFTOrienter.h
//  PithDectection
//
//  Created by Remi DECELLE on 18/01/2019.
//  Copyright © 2019 Remi DECELLE. All rights reserved.
//

#ifndef FFTOrienter_h
#define FFTOrienter_h

#include <opencv2/core.hpp>
#include <opencv2/imgproc.hpp>
#include <vector>
#include <iostream>

#include "IOrienter.h"
#include "math.hpp"
#include "ui.hpp"

using namespace cv;
using namespace std;

/**
 * Implements FFT tree rings local orientation analysis
 * Analysis done by sum value for each angle in fft
 */
class FFTOrienter : public IOrienter
{
private:
    int __sub_window_size;
    int __nbins;
    
public:
    
    FFTOrienter(int swsize, int nbins) :
        __sub_window_size(swsize), __nbins(nbins) {};
    ~FFTOrienter() {};

    double find_angle(Mat window);
    vector<double> find_orientations(Mat neighbor);
    
    /**
     * This function is not require
     */
    vector<double> find_orientations(Mat norm, Mat direction)
    {
        return vector<double>();
    }
    
};
#endif /* FFTOrienter_h */
