//
//  IOrienter.h
//  PithDectection
//
//  Created by Remi DECELLE on 18/01/2019.
//  Copyright © 2019 Remi DECELLE. All rights reserved.
//

#ifndef IOrienter_h
#define IOrienter_h

#include <opencv2/core.hpp>
#include <opencv2/imgproc.hpp>
#include <vector>
using namespace cv;
using namespace std;

/**
 * Implements your own tree rings local orientation analysis
 */
class IOrienter
{
public:
    IOrienter() {};
    ~IOrienter() {};

    virtual vector<double> find_orientations(Mat neighbor) = 0;
    virtual vector<double> find_orientations(Mat norm, Mat direction) = 0;
};

#endif /* IOrienter_h */
