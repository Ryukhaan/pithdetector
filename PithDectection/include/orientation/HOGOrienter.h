//
//  HOGOrienter.h
//  PithDectection
//
//  Created by Remi DECELLE on 28/01/2019.
//  Copyright © 2019 Remi DECELLE. All rights reserved.
//

#ifndef HOGOrienter_h
#define HOGOrienter_h

#include <opencv2/core.hpp>
#include <opencv2/imgproc.hpp>
#include <vector>

#include "IOrienter.h"
#include "math.hpp"

/**
 * Implements simple Histogram of Oriented Gradient tree rings local orientation analysis
 */
class HOGOrienter : public IOrienter
{
private:
    int __sub_window_size;
    int __nbins;
    
public:
    HOGOrienter(int sbsize, int nbins) :
        __sub_window_size(sbsize), __nbins(nbins) {};
    ~HOGOrienter() {};

    vector<double> find_orientations(Mat neighbor);

    /**
     * This function is not require
     */
    vector<double> find_orientations(Mat norm, Mat direction)
    {
        return vector<double>();
    }
};

#endif /* HOGOrienter_h */
