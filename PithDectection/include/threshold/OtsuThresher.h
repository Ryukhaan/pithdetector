//
//  OtsuThresher.h
//  PithDectection
//
//  Created by Remi DECELLE on 11/04/2019.
//  Copyright © 2019 Remi DECELLE. All rights reserved.
//

#ifndef OtsuThresher_h
#define OtsuThresher_h

#include <opencv2/core.hpp>
#include <opencv2/imgproc.hpp>

#include "IThresher.h"

using namespace cv;

/**
 * Implements Otsu threshold method
 * We call THRESH_OTSU from opencv
 */
class OtsuThresher : public IThresher
{
public:
    OtsuThresher() {};
    ~OtsuThresher() {};
    
    Mat threshhold(Mat image)
    {
        Mat image_threshed;
        threshold(image, image_threshed, 0, 1, THRESH_OTSU);
        return image_threshed;
    };
};


#endif /* OtsuThresher_h */
