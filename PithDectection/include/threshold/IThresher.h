//
//  IThresher.h
//  PithDectection
//
//  Created by Remi DECELLE on 18/01/2019.
//  Copyright © 2019 Remi DECELLE. All rights reserved.
//

#ifndef IThresher_h
#define IThresher_h

#include <opencv2/core.hpp>
#include <opencv2/imgproc.hpp>
using namespace cv;

/**
 * Abstract class (Interface) to implements your own threshold method
 */
class IThresher
{    
public:
    IThresher() {};
    ~IThresher() {};
    
    virtual Mat threshhold(Mat image) = 0;
};

#endif /* IThresher_h */
