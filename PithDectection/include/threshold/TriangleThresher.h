//
//  TriangleThresher.h
//  PithDectection
//
//  Created by Remi DECELLE on 18/01/2019.
//  Copyright © 2019 Remi DECELLE. All rights reserved.
//

#ifndef TriangleThresher_h
#define TriangleThresher_h

#include <opencv2/core.hpp>
#include <opencv2/imgproc.hpp>

#include "IThresher.h"

using namespace cv;

/**
 * Implements Triangle threshold method (see Zack et al. 1977)
 * We call THRESH_TRIANGLE from opencv
 */
class TriangleThresher : public IThresher
{
public:
    TriangleThresher() {};
    ~TriangleThresher() {};

    Mat threshhold(Mat image)
    {
        Mat image_threshed;
        threshold(image, image_threshed, 0, 1, THRESH_TRIANGLE);
        return image_threshed;
    };
};

#endif /* TriangleThresher_h */
