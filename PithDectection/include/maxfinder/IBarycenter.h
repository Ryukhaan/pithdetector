//
//  IBarycenter.h
//  PithDectection
//
//  Created by Remi DECELLE on 18/01/2019.
//  Copyright © 2019 Remi DECELLE. All rights reserved.
//

#ifndef IBarycenter_h
#define IBarycenter_h

#include <opencv2/core.hpp>
#include <opencv2/imgproc.hpp>
using namespace cv;

/**
 * Implements your own "maximum" accumulation
 * (Barycenter, Maximum, Region growing, ...)
 */

class IBarycenter {
public:
    IBarycenter() {};
    ~IBarycenter() {};

    virtual Point optimum(Mat intersection) = 0;
};
#endif /* IBarycenter_h */
