//
//  Maximum.h
//  PithDectection
//
//  Created by Remi DECELLE on 18/01/2019.
//  Copyright © 2019 Remi DECELLE. All rights reserved.
//

#ifndef Maximum_h
#define Maximum_h

#include <opencv2/core.hpp>
#include <opencv2/imgproc.hpp>
#include <vector>

#include "IBarycenter.h"

using namespace cv;
using namespace std;

/**
 * Class to retrieve the maximum given an accumulation space
 */
class Maximum : public IBarycenter
{
private:
	// Quantize original image
    int __quantizer;
    
public:
    Maximum(): __quantizer(0) {};
    Maximum(int q): __quantizer(q) {};
    ~Maximum() {};
    
    Point optimum(Mat intersection);
};

#endif /* Maximum_h */
