//
//  Barycenter.h
//  PithDectection
//
//  Created by Remi DECELLE on 18/01/2019.
//  Copyright © 2019 Remi DECELLE. All rights reserved.
//

#ifndef Barycenter_h
#define Barycenter_h

#include <opencv2/core.hpp>
#include <opencv2/imgproc.hpp>

#include "IBarycenter.h"

using namespace cv;

/**
 * Class to retrieve the barycenter of an accumulation space
 */
class Barycenter : public IBarycenter
{
private:
    int __quantizer;
    
public:
    Barycenter() {};
    Barycenter(int q): __quantizer(q) {};
    ~Barycenter() {};

    Point optimum(Mat intersection);
};



#endif /* Barycenter_h */
