//
//  EnergyTreater.hpp
//  PithDectection
//
//  Created by Remi DECELLE on 01/02/2019.
//  Copyright © 2019 Remi DECELLE. All rights reserved.
//

#ifndef EnergyTreater_hpp
#define EnergyTreater_hpp

#include <iostream>
#include <opencv2/core.hpp>
#include <opencv2/imgproc.hpp>

#include "math.hpp"
#include "ITreater.h"

using namespace cv;
using namespace std;

/**
 * TO COMMENT !
 */
class EnergyFilter : public ITreater
{
public:
    int __kernel_size;
    double __sigma;
    
    Mat E5 = (Mat_<double>(5, 1) << -1, -2, 0, 2, 1);
    Mat L5 = (Mat_<double>(5, 1) << 1, 4, 6, 4, 1);
    Mat W5 = (Mat_<double>(5, 1) << -1, 0, 3, 0, -3, 0, 1);
    Mat E7 = (Mat_<double>(7, 1) << -1, -4, -5, 0, 5, 4, 1);
public:
    
    EnergyFilter(double scale, double sigma, int kernel_size) :
        ITreater(scale), __sigma(sigma), __kernel_size(kernel_size) {};
    
    Mat execute(Mat image)
    {
        Mat scale, gray, gauss, result;
        cvtColor(image, gray, COLOR_BGR2GRAY);
        resize(gray, scale, Size(image.cols * __scale, image.rows * __scale), INTER_LINEAR);
        GaussianBlur(scale, gauss, Size(__kernel_size, __kernel_size), __sigma, 0, BORDER_REPLICATE);
        /*
        resize(image, scaled_image, Size(image.cols * __scale, image.rows * __scale), INTER_LINEAR);
        cvtColor(scaled_image, gray, COLOR_BGR2GRAY);
        gauss_image = gray.clone();
        //GaussianBlur(gray, gauss_image, Size(__kernel_size, __kernel_size), __sigma, 0, BORDER_REPLICATE);
        */
        //kernel = E5 * E5t; // Best for Douglas !!
        Mat a = E5.clone();
        Mat b = E5.clone();
        
        //t = W5.clone();
        transpose(b, b);
        
        Mat kernel = Mat::zeros(5, 5, CV_64F);
        kernel = a * b;
        filter2D(gauss, result, -1, kernel, Point( -1, -1 ), 0, BORDER_REPLICATE);
        return result;
    }
    
    Mat compute_gradient_x(Mat image)
    {
        Mat scaled_image, gray, gauss_image, g_x;
        resize(image, scaled_image, Size(image.cols * __scale, image.rows * __scale), INTER_LINEAR);
        cvtColor(scaled_image, gray, COLOR_BGR2GRAY);
        GaussianBlur(gray, gauss_image, Size(__kernel_size, __kernel_size), __sigma, 0, BORDER_REPLICATE);
        Mat E5 = (Mat_<double>(5, 1) << -1, -2, 0, 2, 1);
        Mat L5 = (Mat_<double>(1, 5) << 1, 4, 6, 4, 1);
        Mat kernel = Mat::zeros(5, 5, CV_64F);
        kernel = E5 * L5;
        filter2D(gauss_image, g_x, -1, kernel, Point( -1, -1 ), 0, BORDER_REPLICATE);
        return g_x;
    }
    
    Mat compute_gradient_y(Mat image)
    {
        Mat scaled_image, gray, gauss_image, g_y;
        resize(image, scaled_image, Size(image.cols * __scale, image.rows * __scale), INTER_LINEAR);
        cvtColor(scaled_image, gray, COLOR_BGR2GRAY);
        GaussianBlur(gray, gauss_image, Size(__kernel_size, __kernel_size), __sigma, 0, BORDER_REPLICATE);
        Mat E5 = (Mat_<double>(1, 5) << -1, -2, 0, 2, 1);
        Mat L5 = (Mat_<double>(5, 1) << 1, 4, 6, 4, 1);
        Mat kernel = Mat::zeros(5, 5, CV_64F);
        kernel = L5 * E5;
        filter2D(gauss_image, g_y, -1, kernel, Point( -1, -1 ), 0, BORDER_REPLICATE);
        return g_y;
    }
};


#endif /* EnergyTreater_hpp */
