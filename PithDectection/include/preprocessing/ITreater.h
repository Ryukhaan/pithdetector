//
//  ITreater.h
//  PithDectection
//
//  Created by Remi DECELLE on 18/01/2019.
//  Copyright © 2019 Remi DECELLE. All rights reserved.
//

#ifndef ITreater_h
#define ITreater_h

#include <opencv2/core.hpp>
#include <opencv2/imgproc.hpp>
#include <map>
#include <string>

using namespace cv;
using namespace std;

/**
 * Abstract Class to define a preprocessing step
 */
class ITreater
{
protected:
    double __scale;
    
public:
    ITreater(double scale) : __scale(scale) {};
    
    virtual Mat execute(Mat image) = 0;
    virtual Mat compute_gradient_x(Mat image) = 0;
    virtual Mat compute_gradient_y(Mat image) = 0;

    /**
     * Getters and Setters
     */
    double get_scale() 
    { 
    	return __scale; 
    };

    void set_scale(double scale)
    {
    	__scale = scale;
    }
};

#endif /* ITreater_h */
