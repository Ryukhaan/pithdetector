//
//  DoGTreater.h
//  PithDectection
//
//  Created by Remi DECELLE on 18/01/2019.
//  Copyright © 2019 Remi DECELLE. All rights reserved.
//

#ifndef DoGTreater_h
#define DoGTreater_h

#include <stdio.h>
#include <opencv2/core.hpp>
#include <opencv2/imgproc.hpp>

#include "math.hpp"
#include "ITreater.h"

using namespace cv;
using namespace std;

/**
 * TO COMMENT !
 */
class DoGTreater : public ITreater
{
private:
    double __sigma_high;
    double __sigma_low;
    
public:
    
    DoGTreater(double scale, double sigma_high, double sigma_low) :
        ITreater(scale), __sigma_high(sigma_high), __sigma_low(sigma_low) {};
                 
    Mat execute(Mat image)
    {
        Mat scaled_image, gray;
        resize(image, scaled_image, Size(image.cols * __scale, image.rows * __scale), INTER_LINEAR);
        cvtColor(scaled_image, gray, COLOR_BGR2GRAY);
        return math::difference_of_gaussian(gray, __sigma_high, __sigma_low);
    }
    
    Mat compute_gradient_y(Mat image)
    {
        return Mat::zeros(1,1,CV_8U);
    }
    
    Mat compute_gradient_x(Mat image)
    {
        return Mat::zeros(1,1,CV_8U);
    }
};

#endif /* DoGTreater_h */
