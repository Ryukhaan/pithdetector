//
//  LoGTreatment.h
//  PithDectection
//
//  Created by Remi DECELLE on 18/01/2019.
//  Copyright © 2019 Remi DECELLE. All rights reserved.
//

#ifndef LoGTreatment_h
#define LoGTreatment_h

#include <opencv2/core.hpp>
#include <opencv2/imgproc.hpp>

#include "math.hpp"
#include "ITreater.h"
#include "ui.hpp"

using namespace cv;

/**
 * Implements a preprocessing with three steps:
 *  - Downsampling;
 *  - Keep only blue component;
 *  - Apply a LoG Filter.
 */
class LoGTreater : public ITreater
{
public:
    // Kernel size of Gaussian filter
    int __kernel_size;
    // Standard deviation of Gaussian filter
    double __sigma;
    
public:
    
    LoGTreater(double scale, double sigma, int kernel_size) :
        ITreater(scale), __sigma(sigma), __kernel_size(kernel_size) {};
    
    /**
     * Execute the preprocessing step
     * 
     * @param Original image
     * @return Preprocessed image
     */
    Mat execute(Mat image)
    {
        Mat scaled_image, gray, tmp;
        // 1. Downsampling
        resize(image, scaled_image, Size(image.cols * __scale, image.rows * __scale), INTER_LINEAR);

        // Enhance tree rings by equalize histogram ?
        /*
        Ptr<CLAHE> clahe = createCLAHE();
        clahe->setClipLimit(4.0);
        clahe->apply(gray, gray);
        */
       
        // 2. Keep only blue component
        Mat channel[3];
        split(scaled_image, channel);
        gray = channel[1];
        
        // 3. Apply LoG filter (see math.hpp/math.cpp)
        return math::laplacian_of_gaussian(gray, __sigma, __kernel_size);
        /*
        // Just a gaussian filter ?
        Size kernel = Size(__kernel_size, __kernel_size);
        GaussianBlur(gray, gray, kernel, __sigma, 0, BORDER_REPLICATE);
        //clahe->apply(gray, gray);
        return gray;
         */
    }
    
    /**
     * This function is no require
     */
    Mat compute_gradient_y(Mat image)
    {
        return Mat::zeros(1,1,CV_8U);
    };
    
    /**
     * This function is not require
     */
    Mat compute_gradient_x(Mat image)
    {
        return Mat::zeros(1,1,CV_8U);
    }

    /**
     * Getters and setters
     */
    void set_kernel_size(int kernel_size)
    {
        __kernel_size = kernel_size;
    };

    void set_sigma(double sigma)
    {
        __sigma = sigma;
    };

    int get_kernel_size()
    {
        return __kernel_size;
    };

    double get_sigma()
    {
        return __sigma;
    };
};

#endif /* LoGTreatment_h */
