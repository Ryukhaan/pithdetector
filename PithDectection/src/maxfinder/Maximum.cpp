//
//  Maximum.cpp
//  PithDectection
//
//  Created by Remi DECELLE on 18/01/2019.
//  Copyright © 2019 Remi DECELLE. All rights reserved.
//

#include "Maximum.h"
#include <iostream>
#include "ui.hpp"

Point Maximum::optimum(Mat intersection)
{
    double minVal;
    double maxVal;
    minMaxIdx(intersection, &minVal, &maxVal);
    vector<Point> idx;
    Mat maxima = (intersection >= static_cast<int>(maxVal));
    if (maxVal == 0)
    {
        return Point(-1, -1);
    }
    /*
    findNonZero(maxima, idx);
    if (idx.size() == intersection.rows * intersection.cols)
    {
        return Point(-1,-1);
    }
    */
    // Compute barycenter of all maxima idxs
    Moments m = moments(maxima, true);
    int bx = m.m10 / m.m00;
    int by = m.m01 / m.m00;

    // Dont forget to get back to original size
    return Point( __quantizer * bx, __quantizer * by);
}
