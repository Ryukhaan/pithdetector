//
//  AdjacentModel.cpp
//  PithDectection
//
//  Created by Remi DECELLE on 09/04/2019.
//  Copyright © 2019 Remi DECELLE. All rights reserved.
//

#include "AdjacentModel.hpp"
#include "visitor.hpp"
#include <algorithm>
#include <vector>
#include <random>
#include <chrono>

AdjacentModel::AdjacentModel(ITreater* treater,
                         int grid_size,
                         int nmi,
                         int window_height,
                         int window_width,
                         int fft_window_size,
                         int nbins,
                         double pas,
                         double epsilon,
                         double sigma) : IModel(treater, window_height, window_width, grid_size, nmi)
{
    __fft_window_size   = fft_window_size;
    __nbins             = nbins;
    __pas               = pas;
    __epsilon           = epsilon;
    __sigma             = sigma;
};

AdjacentModel::AdjacentModel(string load_path)
{
    this->load(load_path);
    __window_height   = 0;
    __window_width    = 0;
}

void AdjacentModel::load(string parameters_path)
{
    FileStorage parameters(parameters_path, FileStorage::READ);
    
    
    __treater = new LoGTreater(parameters["SMA"]["Treater"]["scale"],
                               parameters["SMA"]["Treater"]["sigma"],
                               parameters["SMA"]["Treater"]["kernel_size"]);
    
    /*
     __treater = new EnergyFilter(parameters["SMA"]["Treater"]["scale"],
     parameters["SMA"]["Treater"]["sigma"],
     parameters["SMA"]["Treater"]["kernel_size"]);
     */
    __num_max_iter      = parameters["SMA"]["max_iter"];
    __num_agents        = pow(parameters["SMA"]["grid_size"], 2.0);
    __fft_window_size   = parameters["SMA"]["Orienter"]["sub_window_size"];
    __nbins             = parameters["SMA"]["Orienter"]["nbins"];
    __pas               = parameters["SMA"]["Agent"]["pas"];
    __epsilon           = parameters["SMA"]["Agent"]["epsilon"];
    __sigma             = parameters["SMA"]["Agent"]["sigma"];
    __quantizer         = parameters["SMA"]["quantizer"];
}

void AdjacentModel::reinit_agents()
{
    agents.clear();
    
    TriangleThresher* thresher      = new TriangleThresher();
    //FFTOrienter* orienter           = new FFTOrienter(__fft_window_size, __nbins);
    HOGOrienter* orienter           = new HOGOrienter(__fft_window_size, __nbins);
    //LineIntersecter* intersecter    = new LineIntersecter(__fft_window_size);
    PeerLineIntersecter* intersecter = new PeerLineIntersecter(__fft_window_size, __quantizer);
    Maximum* barycenter             = new Maximum(__quantizer);
    
    int grid_size = sqrt(__num_agents);
    int padding = __fft_window_size + ceil(__fft_window_size * 0.5);
    int H = __window_height * __treater->get_scale();
    int W = __window_width * __treater->get_scale();
    //int hg = (H - 3*__fft_window_size) / grid_size;
    //int wg = (W - 3*__fft_window_size) / grid_size;
    int hg = H / grid_size;
    int wg = W / grid_size;
    
    for (int i = 0; i < grid_size; i++)
    {
        for (int j = 0; j < grid_size; j++)
        {
            int cx = i * wg + ceil(wg / 2);
            int cy = j * hg + ceil(hg / 2);
            agents.push_back(new AdjacentAgent(cx, cy, false,
                                             thresher,
                                             orienter,
                                             intersecter,
                                             barycenter,
                                             __pas,
                                             __epsilon,
                                             __sigma,
                                             padding,
                                             H,
                                             W));
        }
    }
}


Point AdjacentModel::execute(Mat image)
{
    //ui::saveas(image, "original_image");
    
    Mat image_treated;
    image_treated = __treater->execute(image);
    
    Mat tmp;
    resize(image, tmp, Size(image.cols * __treater->get_scale(), image.rows * __treater->get_scale()), INTER_LINEAR);

    unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
    vector<int> list;
    for (int i = 0; i < __num_agents; i++) list.push_back(i);
    int width = sqrt(__num_agents);
    
    Mat votation(image_treated.rows, image_treated.cols, CV_64F, double(0));
    int num_iter = 0;
    while (num_iter < __num_max_iter)
    {
        //this->view_agents(image_treated, 100);
        //this->save_agents(tmp, "agents_at_"+to_string(num_iter));
        bool has_converge = true;
        vector<int> lister;
        lister = list;
        std::shuffle(lister.begin(), lister.end(), std::default_random_engine(seed));
//#pragma omp parallel for num_threads(4)
        //for (int i = 0; i < __num_agents; i++)
        for (int i : lister)
        {
            int j = i % width;
            int k = i / width;
            IAgent* agent = agents.at(i);
            if (agent->is_movable())
            {
                has_converge = false;
                Mat distances = Mat::zeros(__num_agents, 1, CV_64F), B;
                distances.at<double>(i, 0) = INFINITY;
//#pragma omp parallel for num_threads(4)
                for (int n = -width; n <= width; n++)
                {
                    for (int m = -width; m <= width; m++)
                    {
                        int ii = (k+n) * width + (j+m);
                        if (ii < 0) continue;
                        if (ii >= __num_agents) continue;
                        if (ii == i) continue;
                        IAgent* aa = agents.at(ii);
                        Point p = agent->getPosition();
                        Point q = aa->getPosition();
                        double distance = pow(p.x - q.x, 2.0) + pow(p.y - q.y, 2.0);
                        distances.at<double>(ii, 0) = distance;
                    }
                }
                agent->move(image_treated);
                agent->update_pas(__pas, num_iter, __num_max_iter);
                sortIdx(distances.t(), B, CV_SORT_ASCENDING);
                for (int n=0; n<4; n++)
                {
                    int ii = B.at<int>(n);
                    double distance = distances.at<double>(ii, 0);
                    //double gamma = min(1.0, exp( - (distance) / (2.0 * pow(__sigma, 2.0))));
                    //double gamma = 1.0;
                    AdjacentAgent* neighbor = (AdjacentAgent*) agents.at(ii);
                    neighbor->move_to(agent->getPosition(), 0.2);
                }
                //this->view_agents(image_treated, 10);
            }
        }
        if (has_converge) break;
        num_iter++;
    }
    
    //this->save_agents(tmp, "agents_final");
    /*
    //Mat votation(image_treated.rows, image_treated.cols, CV_64F, double(0));
    // Votation by probability
    double px = 0, py = 0;
#pragma omp parallel for num_threads(4)
    for (int i = 0; i < __num_agents; i++)
    {
        IAgent* agent = agents.at(i);
        px += agent->getPosition().x;
        py += agent->getPosition().y;
    }
    px = px / __num_agents;
    py = py / __num_agents;
    px = px / __treater->get_scale();
    py = py / __treater->get_scale();
    */

#pragma omp parallel for num_threads(4)
    for (int i = 0; i < __num_agents; i++)
    {
        IAgent* agent = agents.at(i);
        Mat probability = agent->vote();
        votation = votation + probability;
    }
    //ui::imagesc(votation, 0);
    //ui::saveas(votation, "map_probabilty");
    //ui::imagesc(votation, 0);
    double minVal;
    double maxVal;
    Point minLoc, maxLoc;
    minMaxLoc(votation, &minVal, &maxVal, &minLoc, &maxLoc);
    double px = maxLoc.x / __treater->get_scale();
    double py = maxLoc.y / __treater->get_scale();
    return Point(px, py);
}

void AdjacentModel::accept(Visitor &v)
{
    v.visit(this);
}
