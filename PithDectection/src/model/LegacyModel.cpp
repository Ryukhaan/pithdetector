//
//  LegacyModel.cpp
//  PithDectection
//
//  Created by Remi DECELLE on 18/01/2019.
//  Copyright © 2019 Remi DECELLE. All rights reserved.
//

#include "LegacyModel.h"
#include "visitor.hpp"

LegacyModel::LegacyModel(ITreater* treater,
                         int grid_size,
                         int nmi,
                         int window_height,
                         int window_width,
                         int fft_window_size,
                         int nbins,
                         double pas,
                         double epsilon,
                         double sigma) : IModel(treater, window_height, window_width, grid_size, nmi)
{
    __fft_window_size   = fft_window_size;
    __nbins             = nbins;
    __pas               = pas;
    __epsilon           = epsilon;
    __sigma             = sigma;
};

LegacyModel::LegacyModel(string load_path)
{
    this->load(load_path);
    __window_height   = 0;
    __window_width    = 0;
}

void LegacyModel::load(string parameters_path)
{
    FileStorage parameters(parameters_path, FileStorage::READ);
    
    __treater = new LoGTreater(parameters["SMA"]["Treater"]["scale"],
                               parameters["SMA"]["Treater"]["sigma"],
                               parameters["SMA"]["Treater"]["kernel_size"]);
    __num_max_iter      = parameters["SMA"]["max_iter"];
    __num_agents        = pow(parameters["SMA"]["grid_size"], 2.0);
    __fft_window_size   = parameters["SMA"]["Orienter"]["sub_window_size"];
    __nbins             = parameters["SMA"]["Orienter"]["nbins"];
    __pas               = parameters["SMA"]["Agent"]["pas"];
    __epsilon           = parameters["SMA"]["Agent"]["epsilon"];
    __sigma             = parameters["SMA"]["Agent"]["sigma"];
    __quantizer         = parameters["SMA"]["quantizer"];
}

void LegacyModel::reinit_agents()
{
    agents.clear();
    
    TriangleThresher* thresher      = new TriangleThresher();
    //FFTOrienter* orienter           = new FFTOrienter(__fft_window_size, __nbins);
    HOGOrienter* orienter           = new HOGOrienter(__fft_window_size, __nbins);
    //LineIntersecter* intersecter    = new LineIntersecter(__fft_window_size);
    PeerLineIntersecter* intersecter = new PeerLineIntersecter(__fft_window_size, __quantizer);
    Maximum* barycenter             = new Maximum(__quantizer);
    
    int grid_size = sqrt(__num_agents);
    int padding = __fft_window_size + ceil(__fft_window_size * 0.5);
    int H = __window_height * __treater->get_scale();
    int W = __window_width * __treater->get_scale();
    //int hg = (H - 3*__fft_window_size) / grid_size;
    //int wg = (W - 3*__fft_window_size) / grid_size;
    int hg = H / grid_size;
    int wg = W / grid_size;
    
    for (int i = 0; i < grid_size; i++)
    {
        for (int j = 0; j < grid_size; j++)
        {
            int cx = i * wg + ceil(wg / 2);
            int cy = j * hg + ceil(hg / 2);
            agents.push_back(new LegacyAgent(cx, cy, false,
                                             thresher,
                                             orienter,
                                             intersecter,
                                             barycenter,
                                             __pas,
                                             __epsilon,
                                             __sigma,
                                             padding,
                                             H,
                                             W));
        }
    }
}


Point LegacyModel::execute(Mat image)
{
    Mat image_treated;
    image_treated = __treater->execute(image);
    
    Mat tmp;
    resize(image, tmp, Size(image.cols * __treater->get_scale(), image.rows * __treater->get_scale()), INTER_LINEAR);

    int num_iter = 0;
    while (num_iter < __num_max_iter)
    {
        // Uncomment to this step by step
        //this->view_agents(image_treated, 0);
        bool has_converge = true;
#pragma omp parallel for num_threads(4)
        for (int i = 0; i < __num_agents; i++)
        {
            IAgent* agent = agents.at(i);
            if (agent->is_movable())
            {
                has_converge = false;
                agent->move(image_treated);
                agent->update_pas(__pas, num_iter, __num_max_iter);
            }
        }
        if (has_converge) break;
        num_iter++;
    }

    Mat votation(image_treated.rows, image_treated.cols, CV_64F, double(0));
    // Votation by probability
#pragma omp parallel for num_threads(4)
    for (int i = 0; i < __num_agents; i++)
    {
        IAgent* agent = agents.at(i);
        Mat probability = agent->vote();
        votation = votation + probability;
    }
    
    double minVal;
    double maxVal;
    Point minLoc, maxLoc;
    minMaxLoc(votation, &minVal, &maxVal, &minLoc, &maxLoc);
    double px = maxLoc.x / __treater->get_scale();
    double py = maxLoc.y / __treater->get_scale();
    
    return Point(px, py);
}

void LegacyModel::accept(Visitor &v)
{
    v.visit(this);
}
