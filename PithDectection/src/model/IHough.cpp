//
//  IHough.cpp
//  PithDectection
//
//  Created by Remi DECELLE on 31/01/2019.
//  Copyright © 2019 Remi DECELLE. All rights reserved.
//

#include "IHough.hpp"
#include "LoGTreater.h"
#include "EnergyTreater.hpp"
#include "ui.hpp"

#include <omp.h>

IHough::IHough(string load_path)
{
    this->load(load_path);
    __starter = Point(0,0);
}

Point IHough::estimate(Mat image)
{
    Mat gray, grad;
    Mat edge;
    double scale = __treater->get_scale();
    
    edge = __treater->execute(image);
    
    int H = edge.rows;
    int W = edge.cols;
    
    // Compute Gradient and orientation
    Mat Gx, Gy, O;
    int ddepth = CV_64F;
    Sobel(edge, Gx, ddepth, 1, 0, 9, 1, 0, BORDER_REPLICATE);
    Sobel(edge, Gy, ddepth, 0, 1, 9, 1, 0, BORDER_REPLICATE);
    assert(Gx.size == Gy.size);
    phase(Gx, Gy, O);
    
    double distance = INFINITY;
    int old_px, old_py;
    int n_iter = 0;
    int px = __starter.x;
    int py = __starter.y;
    while (distance > __epsilon && n_iter < __max_iter) {
        
        n_iter++;
        old_px = px;
        old_py = py;
        
        // Get sub-window
        int sx = ceil(max(0.0, px * scale - __window_size));
        int sy = ceil(max(0.0, py * scale - __window_size));
        int ey = ceil(min(static_cast<double>(H-1), py * scale + __window_size));
        int ex = ceil(min(static_cast<double>(W-1), px * scale + __window_size));
        
        // Get egde and orientation sub-window
        Mat sub_direction, sub_edge;
        O(Range(sy, ey), Range(sx, ex)).copyTo(sub_direction);
        edge(Range(sy, ey), Range(sx, ex)).copyTo(sub_edge);
        
        Mat tmp;
        resize(image, tmp, Size(image.cols * scale, image.rows * scale), INTER_LINEAR);
        tmp(Range(sy,ey), Range(sx,ex)).copyTo(tmp);

        Mat bw;
        threshold(sub_edge, bw, 0, 1, THRESH_TRIANGLE);
        
        int length = 2 * __window_size;
        Mat A = Mat::zeros(length, length, CV_8U);
#pragma omp parallel for collapse(2)
        for (int j=0; j<length; j++) {
            for (int i=0; i<length; i++) {
                if ( ! bw.at<bool>(j,i) ) continue;
                double a = tan(sub_direction.at<double>(j,i));
                auto f = [](const int x, const double a, const int cx, const int cy) { return a*(x-cx) + cy;};
                auto g = [](const int y, const double a, const int cx, const int cy) { return (y-cy)/a + cx;};
                Mat x(4, 1, CV_64F, {-1,-1,-1,-1});
                Mat y(4, 1, CV_64F, {-1,-1,-1,-1});
                
                if (abs(a) > 0) {
                    x.at<double>(0,0) = g(0, a, i, j);
                    y.at<double>(0,0) = 0;
                    x.at<double>(2,0) = g(length-1, a, i, j);
                    y.at<double>(2,0) = length-1;
                }
                if (a < 1e18) {
                    x.at<double>(1,0) = 0;
                    y.at<double>(1,0) = f(0, a, i, j);
                    x.at<double>(3,0) = length-1;
                    y.at<double>(3,0) = f(length-1, a, i, j);
                }
                Point starter, ender;
                Mat P = (x >= 0) & (x < bw.rows) & (y >= 0) & (y < bw.cols);
                P &= 0x1;
                vector<int> p;
                for (int m=0; m<P.rows; m++)
                    if (P.at<bool>(m,0))
                        p.push_back(m);
                
                if (p.size() < 2) continue;
                
                starter = Point(x.at<double>(p[0],0), y.at<double>(p[0],0));
                ender = Point(x.at<double>(p[1],0), y.at<double>(p[1],0));
                
                LineIterator it(A, starter, ender);
                for (int k = 0; k<it.count; k++, ++it)
                    A.at<uchar>(it.pos())++;
            }
        }
        
        // Median filter to reduce peaks in accumulation's space
        A.convertTo(A, CV_8U);
        medianBlur(A, A, __median_filter_size);
        
        // Get maximum value
        double minVal;
        double maxVal;

        // Compute barycenter
        minMaxIdx(A, &minVal, &maxVal);
        vector<int> bxs;
        vector<int> bys;
        
        Mat B = A >= (__ratio * maxVal);
        for (int row = 0; row<A.rows; row++) {
            bool* current_row = B.ptr<bool>(row);
            for (int col = 0; col<A.cols; col++) {
                if ( current_row[col] ) {
                    bxs.push_back(col);
                    bys.push_back(row);
                }
            }
        }
        assert( bxs.size() > 0);
        // Compute the nearest intersection from barycenter
        Mat Bx = Mat(bxs);
        Mat By = Mat(bys);
        Scalar mx = sum(Bx) / Bx.rows;
        Scalar my = sum(By) / By.rows;
        
        Mat pbx, pby;
        pow(Bx - mx[0], 2, pbx);
        pow(By - my[0], 2, pby);
        Mat distances = pbx + pby;
        Point minLoc, maxLoc;
        minMaxLoc(distances, &minVal, &maxVal, &minLoc, &maxLoc);
        int bx = bxs.at(minLoc.y);
        int by = bys.at(minLoc.y);
        
        
        px = ceil((sx + bx) / scale);
        py = ceil((sy + by) / scale);
        
        distance = sqrt(pow(px-old_px, 2.0) + pow(py-old_py, 2.0));
    }
    
    return Point(px, py);
}

void IHough::load(string parameters_path)
{
    FileStorage parameters(parameters_path, FileStorage::READ);
    
    __treater = new LoGTreater(parameters["Hough"]["Treater"]["scale"],
                               parameters["Hough"]["Treater"]["sigma"],
                               parameters["Hough"]["Treater"]["kernel_size"]);
    __epsilon               = parameters["Hough"]["epsilon"];
    __window_size           = parameters["Hough"]["window_size"];
    __median_filter_size    = parameters["Hough"]["medfilt_size"];
    __ratio                 = parameters["Hough"]["ratio"];
    __max_iter              = parameters["Hough"]["max_iter"];
}
