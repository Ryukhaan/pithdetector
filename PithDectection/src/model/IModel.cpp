//
//  IModel.cpp
//  PithDectection
//
//  Created by Remi DECELLE on 22/01/2019.
//  Copyright © 2019 Remi DECELLE. All rights reserved.
//

#include <opencv2/plot.hpp>
#include <opencv2/highgui.hpp>
#include "IModel.h"

void IModel::view_agents(Mat image, int wait=10)
{
    double scale = 0.4;
    Mat display = image.clone();
    if (display.channels() == 1)
        cvtColor(display, display, CV_GRAY2BGR);
    if (display.channels() > 2)
        resize(display, display, Size(scale * display.cols, scale * display.rows));
    for (int i = 0; i < __num_agents; i++)
    {
        IAgent* agent = agents.at(i);
        //circle(display, Point(agent->getPosition()), 5, Scalar(0,0,255), -1);
         drawMarker(display, scale * Point(agent->getPosition()), Scalar(255,127,50), MARKER_CROSS, 32, 2);
    }
    //imshow("Debug Model - View Agents", display);
    ui::imagesc(display, wait);
    
    //string image_path = "/Users/remidecelle/Documents/agents.jpg";
    //imwrite(image_path, display);
    //waitKey(0);
}

void IModel::save_agents(Mat image, string filename)
{
    Mat display = image.clone();
    if (display.channels() == 1)
        cvtColor(display, display, CV_GRAY2BGR);
    for (int i = 0; i < __num_agents; i++)
    {
        IAgent* agent = agents.at(i);
        drawMarker(display, Point(agent->getPosition()), Scalar(255,127,50), MARKER_CROSS, 32, 2);
    }
    ui::saveas(display, filename);
}
