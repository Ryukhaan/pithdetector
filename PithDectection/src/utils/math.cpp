//
//  math.cpp
//  PithDectection
//
//  Created by Remi DECELLE on 18/01/2019.
//  Copyright © 2019 Remi DECELLE. All rights reserved.
//

#include <opencv2/core.hpp>
#include <opencv2/imgproc.hpp>
#include <stdio.h>
#include <iostream>
#include "math.hpp"

using namespace cv;
using namespace std;

namespace math {
    
    inline Size gaussian_kernel(double sigma)
    {
        int ksize = 2 * ceil( 2 * sigma ) + 1;
        return Size(ksize, ksize);
    }
    
    Mat linspace(double startP, double Endp, int interval)
    {
        double spacing = interval-1;
        Mat y(spacing, 1, CV_64FC1);
        for (int i = 0; i < y.rows; ++i)
            y.at<double>(i) = startP + i*(Endp - startP)/spacing;
        
        return y;
    }
    
    Mat difference_of_gaussian(Mat input, double sigma_high, double sigma_low)
    {
        Mat f_low, f_high;
        GaussianBlur(input, f_low, gaussian_kernel(sigma_low), sigma_low, 0, BORDER_REPLICATE);
        GaussianBlur(input, f_high, gaussian_kernel(sigma_high), sigma_high, 0, BORDER_REPLICATE);
        return f_high - f_low;
    }
    
    Mat laplacian_of_gaussian(Mat input, double sigma, int kernel_size)
    {
        Mat filtered;
        Size kernel = Size(kernel_size, kernel_size);
        GaussianBlur(input, filtered, kernel, sigma, 0, BORDER_REPLICATE);
        Laplacian(filtered, filtered, -1, 3, 1, 0, BORDER_REPLICATE);
        return filtered;
    }
    
    Mat fft2(Mat input)
    {
        Mat padded;
        int M = getOptimalDFTSize( input.rows );
        int N = getOptimalDFTSize( input.cols );
        
        copyMakeBorder(input, padded, 0, M - input.rows, 0, N - input.cols, BORDER_CONSTANT, Scalar::all(0));
        
        Mat planes[] = {Mat_<double>(padded), Mat::zeros(padded.size(), CV_64F)};
        Mat complexImg, mag;
        merge(planes, 2, complexImg);
        dft(complexImg, complexImg, CV_DXT_FORWARD, 0);
        
        // compute log(1 + sqrt(Re(DFT(img))**2 + Im(DFT(img))**2))
        split(complexImg, planes);
        magnitude(planes[0], planes[1], planes[0]);
        mag = planes[0];
        mag += Scalar::all(1);
        log(mag, mag);
        
        // crop the spectrum, if it has an odd number of rows or columns
        mag = mag(Rect(0, 0, mag.cols & -2, mag.rows & -2));
        
        int cx = mag.cols/2;
        int cy = mag.rows/2;
        
        // rearrange the quadrants of Fourier image
        // so that the origin is at the image center (fftshift)
        Mat tmp;
        Mat q0(mag, Rect(0, 0, cx, cy));
        Mat q1(mag, Rect(cx, 0, cx, cy));
        Mat q2(mag, Rect(0, cy, cx, cy));
        Mat q3(mag, Rect(cx, cy, cx, cy));
        
        q0.copyTo(tmp);
        q3.copyTo(q0);
        tmp.copyTo(q3);
        
        q1.copyTo(tmp);
        q2.copyTo(q1);
        tmp.copyTo(q2);
        
        return mag;
    }
    
    double triangle_thresh(Mat image)
    {
        const int nbins = 256;
        int h[nbins];
        for (int i = 0; i<nbins; i++)
            h[i] = 0;
        
        // Compute histogram
        for (int i = 0; i < image.rows; i++)
        {
            u_int8_t* row = image.ptr<u_int8_t>(i);
            for (int j = 0; j < image.cols; j ++)
            {
                u_int8_t value = row[j];
                h[value]++;
            }
        }
        
        // Find maximum value and indice, left-bound and right-bound
        int left_bound = 0, right_bound = 0, max_value = 0, max_ind = 0;
        // Maximum value and index
        for (int i = 0; i<nbins; i++)
        {
            if (h[i] > max_value)
            {
                max_value = h[i];
                max_ind = i;
            }
        }
        // First non-zero
        for (int i = 0; i<nbins; i++)
        {
            if (h[i] > 0)
            {
                left_bound = i;
                break;
            }
        }
        // Last non-zero
        for (int i = nbins-1; i>0; i--)
        {
            if (h[i]  > 0)
            {
                right_bound = i;
                break;
            }
        }
        bool is_flip = false;
        int a = 0, b = 0;
        int lspan = max_ind - left_bound;
        int rspan = right_bound - max_ind;
        if (rspan > lspan)
        {
            is_flip = true;
            int i = 0, j = nbins-1;
            // Flip histogram
            while ( i < j )
            {
                int tmp = h[i];
                h[i] = h[j];
                h[j] = tmp;
                i++;
                j--;
            }
            a = nbins - right_bound + 1;
            b = nbins - max_ind + 1;
        }
        else
        {
            a = left_bound;
            b = max_ind;
        }
        
        double m = max_value / (b - a);
        double max_dist = 0.0;
        double level = 0;
        for (int i = a; i <= b; i++)
        {
            double beta = h[i] + (i-a) / m;
            double x2 = beta / (m + 1/m);
            double y2 = m * x2;
            double dist = pow(y2 - h[i], 2.0) + pow(x2 - (i-a), 2.0);
            if (dist > max_dist)
            {
                max_dist = dist;
                level = i;
            }
        }
        return is_flip ? (nbins-level+1) / nbins : level / nbins;
    }

}
