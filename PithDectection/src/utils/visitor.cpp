//
//  visitor.cpp
//  PithDectection
//
//  Created by Remi DECELLE on 09/04/2019.
//  Copyright © 2019 Remi DECELLE. All rights reserved.
//

#include "visitor.hpp"

void Visitor::visit(LegacyModel* model)
{
    cout << "\t Preprocess Parameters" << endl;
    cout << "Sampling Factor : " << model->get_treater_scale() << endl;
    cout << "Sigma Value : " << model->get_treater_sigma() << endl;
    cout << "kernel Size : " << model->get_treater_kernel() << endl;
    cout << endl;
    
    cout << "\t Global Parameters" << endl;
    cout << "Maximum iterations : " << model->get_num_max_iter() << endl;
    cout << "Number of Agents :" << model->get_number_of_agents() << endl;
    cout << "Quantizing Factor for Intersection :" << model->get_quantizer() << endl;
    
}

void Visitor::visit(AdjacentModel* model)
{
    cout << "\t Preprocess Parameters" << endl;
    cout << "Sampling Factor : " << model->get_treater_scale() << endl;
    cout << "Sigma Value : " << model->get_treater_sigma() << endl;
    cout << "kernel Size : " << model->get_treater_kernel() << endl;
    cout << endl;
    
    cout << "\t Global Parameters" << endl;
    cout << "Maximum iterations : " << model->get_num_max_iter() << endl;
    cout << "Number of Agents :" << model->get_number_of_agents() << endl;
    cout << "Quantizing Factor for Intersection :" << model->get_quantizer() << endl;
}
