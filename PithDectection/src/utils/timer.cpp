//
//  timer.cpp
//  PithDectection
//
//  Created by Remi DECELLE on 18/01/2019.
//  Copyright © 2019 Remi DECELLE. All rights reserved.
//

#include <opencv2/highgui.hpp>
#include "timer.hpp"
#include <iostream>
namespace timer
{
    
    double tt_tic = 0;
    double tt_toc = 0;
    
    void tic()
    {
        tt_tic = cv::getTickCount();
    }
    
    void toc()
    {
        tt_toc = (cv::getTickCount() - tt_tic)/(cv::getTickFrequency());
        //printf ("\t Time elapsed: %4.3f \n", tt_toc);
        std::cout << "\t" << "Time: " << tt_toc << std::endl;
    }
    
    double get_toc()
    {
        return tt_toc;
    }
    
    double get_tic()
    {
        return tt_tic;
    }
}
