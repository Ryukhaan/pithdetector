//
//  ui.cpp
//  PithDectection
//
//  Created by Remi DECELLE on 18/01/2019.
//  Copyright © 2019 Remi DECELLE. All rights reserved.
//

#include <opencv2/core.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/plot.hpp>
#include <opencv2/highgui.hpp>
#include <stdio.h>
#include <iostream>
#include <string>
#include <fstream>
#include <sstream>
#include "ui.hpp"

using namespace std;
using namespace cv;

namespace ui
{
    
    double scale_imagesc = 1.0;
    
    void imagesc(Mat input, int waiting)
    {
        cv::Mat copycat = input.clone();
        std::string name_window = "Imagesc";
        namedWindow(name_window, cv::WINDOW_AUTOSIZE);
        normalize(copycat, copycat, 0.0, 1.0, cv::NORM_MINMAX, CV_64F);
        copycat.convertTo(copycat, CV_8UC1, 255.0, 0.0);
        //applyColorMap(copycat, copycat, COLORMAP_RAINBOW);
        resize(copycat, copycat, cv::Size(scale_imagesc * copycat.cols, scale_imagesc * copycat.rows), 0, 0, cv::INTER_NEAREST);
        imshow(name_window, copycat);
        int c;
        if (waiting)
        {
            waitKey(waiting);
        }
        else
        {
            c = waitKey(1);
            while ((char)c != 'q')
            {
                c = waitKey(1);
                // Introduce Switch-case
                if ((char)c == 'u')
                {
                    resize(copycat, copycat, cv::Size(2 * copycat.cols, 2 * copycat.rows), 0, 0, cv::INTER_NEAREST);
                }
                else if ((char)c == 'd')
                {
                    resize(copycat, copycat,  cv::Size(0.5 * copycat.cols, 0.5 * copycat.rows), 0, 0, cv::INTER_NEAREST);
                }
                imshow(name_window, copycat);
            }
        }
    }
    
    std::vector<double> read_csv(string path,  string* input)
    {
        std::ifstream reader(path);
        std::string current_line;
        std::vector<double> datas;
        int j = 0;
        while ( getline(reader, current_line) )
        {
            if ( !j )
            {
                j++;
                continue;
            }
            //double line[4];
            std::stringstream tmp(current_line);
            std::string single_value;
            int i = 0;
            while ( getline(tmp, single_value, ';'))
            {
                if ( !i )
                {
                    input[j-1] = single_value;
                    i++;
                }
                else
                {
                    //line[i] = atof(single_value.c_str());
                    datas.push_back(atof(single_value.c_str()));
                    i++;
                }
            }
            j++;
            //datas.push_back(line);
        }
        return datas;
        //return Mat(datas);
    }
    
    std::vector<double> read_pith(string path)
    {
        std::ifstream reader(path);
        std::string current_line;
        std::vector<double> datas;
        int j = 0;
        while ( getline(reader, current_line) )
        {
            std::stringstream tmp(current_line);
            std::string single_value;
            int i = 0;
            while ( getline(tmp, single_value, ','))
            {
                if ( !i )
                {
                    i++;
                }
                else
                {
                    datas.push_back(atof(single_value.c_str()));
                    i++;
                }
            }
            j++;
        }
        return datas;
    }
    
    void saveas(Mat input, string filename)
    {
        string path = "/Users/remidecelle/Documents/Writings/images/" + filename + ".png";
        //string image_path = "/Users/remidecelle/Documents/agents.jpg";
        imwrite(path, input);
    }
}
