//
//  factory.cpp
//  PithDectection
//
//  Created by Remi DECELLE on 11/04/2019.
//  Copyright © 2019 Remi DECELLE. All rights reserved.
//

#include "factory.hpp"
#include "LegacyModel.h"
#include "AdjacentModel.hpp"
#include "IHough.hpp"

using namespace factory;

AbstractModel* ModelFactory::create(model_type type, std::string loading_path)
{
    switch (type) {
        case LegacyModelType:
            return new LegacyModel(loading_path);
        case AdjacentModelType:
            return new AdjacentModel(loading_path);
        case IHoughType:
            return new IHough(loading_path);
        default:
            return nullptr;
    }
}

