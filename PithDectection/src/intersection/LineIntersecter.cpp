//
//  LineIntersecter.cpp
//  PithDectection
//
//  Created by Remi DECELLE on 18/01/2019.
//  Copyright © 2019 Remi DECELLE. All rights reserved.
//

#include "LineIntersecter.h"

Mat LineIntersecter::intersect(vector<double> orientations, int dx, int dy, Mat image)
{
    int height  = image.rows;
    int width   = image.cols;
    Mat A       = Mat::zeros(height, width, CV_64F);
    
    for (int idx = 0; idx < orientations.size(); idx++)
    {
        int i = idx % 3;
        int j = idx / 3;
        double a = tan(orientations[idx]);
        int cx = i * __sub_window_size + (__sub_window_size/2) + dx;
        int cy = j * __sub_window_size + (__sub_window_size/2) + dy;
        auto f = [](int x, double a, int cx, int cy) {return a * (x-cx) + cy;};
        auto g = [](int x, double a, int cx, int cy) {return (x-cy) / a + cx;};
        Mat x(4, 1, CV_64F, {-1,-1,-1,-1});
        Mat y(4, 1, CV_64F, {-1,-1,-1,-1});
        
        if (abs(a) > 0)
        {
            x.at<double>(0,0) = g(0, a, cx, cy);
            y.at<double>(0,0) = 0;
            x.at<double>(2,0) = g(height-1, a, cx, cy);
            y.at<double>(2,0) = height-1;
        }
        if (a < 1e18)
        {
            x.at<double>(1,0) = 0;
            y.at<double>(1,0) = f(0, a, cx, cy);
            x.at<double>(3,0) = width-1;
            y.at<double>(3,0) = f(width-1, a, cx, cy);
        }
        
        Point starter, ender;
        Mat P = (x >= 0) & (x < width) & (y >= 0) & (y < height);
        P &= 0x1;
        vector<int> p;
        for (int m=0; m<P.rows; m++)
        {
            bool* value = P.ptr<bool>(m);
            if (value[0])
            {
                p.push_back(m);
            }
        }
        if (p.size() < 2) continue;
        
        starter = Point(x.at<double>(p[0],0), y.at<double>(p[0],0));
        ender = Point(x.at<double>(p[1],0), y.at<double>(p[1],0));
        
        LineIterator it(A, starter, ender);
        for (int k = 0; k<it.count; k++, ++it)
            A.at<double>(it.pos())++;
        
    }
    return A;
}
