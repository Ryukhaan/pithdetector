//
//  PeerLineIntersecter.cpp
//  PithDectection
//
//  Created by Remi DECELLE on 06/02/2019.
//  Copyright © 2019 Remi DECELLE. All rights reserved.
//

#include "PeerLineIntersecter.hpp"
#include "ui.hpp"

Mat PeerLineIntersecter::intersect(vector<double> orientations, int dx, int dy, Mat image)
{
    int height  = image.rows;
    int width   = image.cols;
    
    vector<double> line_coef;
    for (int idx = 0; idx < orientations.size(); idx++)
    {
        int i = idx % 3;
        int j = idx / 3;
        double a = tan(orientations[idx]);
        int cx = i * __sub_window_size + (__sub_window_size/2) + dx;
        int cy = j * __sub_window_size + (__sub_window_size/2) + dy;
        
        double b = cy - cx * a;
        line_coef.push_back(a);
        line_coef.push_back(b);
    }
    
    Mat A       = Mat::zeros(height / __quantizer, width / __quantizer, CV_64F);
    for (int i = 0; i < line_coef.size(); i+=2)
    {
            for (int j = 0; j < line_coef.size(); j+=2)
            {
                if (i == j) continue;
                double a = line_coef[i];
                double b = line_coef[i+1];
                double c = line_coef[j];
                double d = line_coef[j+1];
                
                if (a == c) continue;
                
                int ix = (d - b) / (a - c);
                int iy = (a * (d - b) / (a - c)) + b;
                if (ix >= 0 && ix < width && iy >= 0 && iy < height)
                {
                    ix = cvFloor(ix / __quantizer);
                    iy = cvFloor(iy / __quantizer);
                    if (iy == A.rows) iy = A.rows-1;
                    A.at<double>(iy, ix)++;
                }
            }
    }
    //ui::saveas(A, "intersect_lines");
    return A;
}
