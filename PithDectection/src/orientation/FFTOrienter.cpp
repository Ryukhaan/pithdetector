//
//  FFTOrientation.cpp
//  PithDectection
//
//  Created by Remi DECELLE on 18/01/2019.
//  Copyright © 2019 Remi DECELLE. All rights reserved.
//

#include "FFTOrienter.h"

using namespace cv;
using namespace std;

double FFTOrienter::find_angle(Mat window)
{
    // Compute FFT
    Mat mag = math::fft2(window);
    int cx = mag.cols/2;
    int cy = mag.rows/2;
    
    //ui::imagesc(mag, 0);
    
    Mat angles = math::linspace(0, CV_PI, __nbins);
    angles += Scalar::all(CV_PI / 10.0);
    double ymax = 0.0;
    double old_summag = -1;
    
    //Mat y(angles.rows, 1, CV_64F);
    for (int i = 0; i < angles.rows; i++)
    {
        int sx = 0, sy = 0, ex, ey;
        double theta = angles.at<double>(i, 0);
        auto f = [](int x, double theta, int cx, int cy) { return tan(theta)*(x-cx)+cy;};
        auto g = [](int y, double theta, int cx, int cy) { return (y-cy)/tan(theta)+cx;};
        if (theta >= 0 && theta <= CV_PI/4.0)
        {
            sx = 0;
            sy = f(0, theta, cx, cy);
        }
        if (theta > CV_PI/4.0 && theta <= 3.0*CV_PI/4.0)
        {
            sx = g(0, theta, cx, cy);
            sy = 0;
        }
        if (theta > 3.0*CV_PI/4.0 && theta <= 5*CV_PI/4.0)
        {
            sx = mag.rows-1;
            sy = f(mag.rows-1, theta, cx, cy);
        }
        ex = cx;
        ey = cy;
        Point starter(sx, sy);
        Point ender(ex, ey);
        
        // Bresenham discrete line
        LineIterator it(mag, starter, ender);
        double summag = 0;
        for (int j=0; j<it.count; j++, ++it)
            summag += mag.at<double>(it.pos());
        
        summag = summag / it.count;
        //y.at<double>(i,0) = summag;
        if (summag > old_summag) {
            old_summag = summag;
            ymax = i;
        }
    }
    //cout << y.t() << endl;
    return CV_PI * ymax / __nbins;
};

vector<double> FFTOrienter::find_orientations(Mat neighbor)
{
    vector<double> orientations;
    for (int fy = 0; fy < neighbor.rows; fy += __sub_window_size)
    {
        for (int fx = 0; fx < neighbor.cols; fx += __sub_window_size)
        {
            Rect rect = Rect(fx, fy, __sub_window_size, __sub_window_size);
            orientations.push_back(find_angle(neighbor(rect)));
        }
    }
    return orientations;
};
