//
//  HOGOrienter.cpp
//  PithDectection
//
//  Created by Remi DECELLE on 28/01/2019.
//  Copyright © 2019 Remi DECELLE. All rights reserved.
//

#include "HOGOrienter.h"
#include <stdio.h>
#include <iostream>
#include "ui.hpp"

using namespace std;
using namespace cv;

vector<double> HOGOrienter::find_orientations(Mat neighbor)
{
    Mat Gx, Gy;
    Mat M, O;
    int ddepth = CV_64F;
    vector<double> orientations;
    int count = 1;
    for (int fy = 0; fy < neighbor.rows; fy += __sub_window_size)
    {
        for (int fx = 0; fx < neighbor.cols; fx += __sub_window_size)
        {
            double angle;
            Rect rect = Rect(fx, fy, __sub_window_size, __sub_window_size);
            Mat sub_window = neighbor(rect);
            // Compute Gradient with Sobel mask
            Sobel(sub_window, Gx, ddepth, 1, 0, 3);
            Sobel(sub_window, Gy, ddepth, 0, 1, 3);
            // Compute gradient's direction
            phase(Gx, Gy, O);
            sqrt(Gx.mul(Gx) + Gy.mul(Gy), M);
            
            //ui::saveas(sub_window, "sub_neighbor_" + to_string(count));
            //ui::saveas(M, "sub_orient_" + to_string(count));
            //ui::saveas(O, "sub_grad_" + to_string(count));
            //count++;
            //M = (Gx.mul(Gx) + Gy.mul(Gy));
            //cartToPolar(Gx, Gy, M, O);
            //ui::imagesc(O, 0);
            //ui::imagesc(sub_window, 0);
            
            Mat histo = Mat(1, __nbins, CV_16U, int(0));
            for (int row = 0; row < O.rows; row++)
            {
                double* o_row = O.ptr<double>(row);
                double* m_row = M.ptr<double>(row);
                for (int col = 0; col < O.cols; col++)
                {
                    double direction = o_row[col];
                    double magnitude = m_row[col];
                    
                    
                    if (direction > CV_PI)
                    {
                        direction -= CV_PI;
                    }
                    double nums = direction * __nbins / CV_PI;
                    
                    //double nums = direction * __nbins / (2 * CV_PI);
                    int inf_ith_bin = cvFloor(nums);
                    int sup_ith_bin = (inf_ith_bin + 1) % __nbins;
                    double coef = nums - inf_ith_bin;
                    assert(inf_ith_bin < __nbins && inf_ith_bin >= 0);
                    assert(sup_ith_bin < __nbins && sup_ith_bin >= 0);
                    assert(coef >= 0.0 && coef <= 1.0);
                    histo.at<uint16_t>(inf_ith_bin) += (1.0-coef) * magnitude;
                    histo.at<uint16_t>(sup_ith_bin) += coef * magnitude;
                }
            }
            //cout << histo << endl;
            // Get maximum value
            double minVal;
            double maxVal;
            Point minPos, maxPos;
            minMaxLoc(Mat(histo), &minVal, &maxVal, &minPos, &maxPos);
            angle = maxPos.x * CV_PI / __nbins;
            //cout << angle << endl;
            orientations.push_back(angle);
        }
    }
    //cout << endl;
    return orientations;
}

