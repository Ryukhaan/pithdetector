//
//  AdjacentAgent.cpp
//  PithDectection
//
//  Created by Remi DECELLE on 09/04/2019.
//  Copyright © 2019 Remi DECELLE. All rights reserved.
//

#include "AdjacentAgent.hpp"
AdjacentAgent::AdjacentAgent(int x,
                         int y,
                         bool stopped,
                         IThresher* thresher,
                         IOrienter* orienter,
                         IIntersecter* intersecter,
                         IBarycenter* barycenter,
                         double pas,
                         double epsilon,
                         double sigma,
                         int padding,
                         int H,
                         int W) : IAgent(x, y, stopped, thresher, orienter, intersecter, barycenter), H(H), W(W)
{
    this->pas         = pas;
    this->epsilon     = epsilon;
    this->sigma_vote  = sigma;
    this->padding     = padding;
}

Mat AdjacentAgent::vote()
{
    Mat X(H, W, CV_64F);
    Mat Y(H, W, CV_64F);
    for (int j = 0; j<H; j++)
    {
        double* xrow = X.ptr<double>(j);
        double* yrow = Y.ptr<double>(j);
        for (int i = 0; i<W; i++)
        {
            xrow[i] = i;
            yrow[i] = j;
        }
    }
    
    Mat A = Mat::zeros(H, W, CV_64F);
    Mat tmpx = X.clone();
    Mat tmpy = Y.clone();
    pow(X-x, 2, tmpx);
    pow(Y-y, 2, tmpy);
    Mat tmp;
    exp(-(tmpx+tmpy) / (2.0 * pow(sigma_vote, 2.0)), tmp);
    return tmp;
}

void AdjacentAgent::update(Point p)
{
    
    double delta = sqrt((pow(x-p.x, 2) + pow(y-p.y, 2)));
    if ( delta < 1 )
    {
        is_stopped = true;
    }
    /*
    int new_x = pas * (p.x - x) + x;
    int new_y = pas * (p.y - y) + y;
    
    if (p.x == x && p.y == y)
    {
        is_stopped = true;
    }
    */
    x = max(min(p.x, W-padding),1+padding);
    y = max(min(p.y, H-padding),1+padding);
    //x = new_x;
    //y = new_y;
}

void AdjacentAgent::move_to(Point p, double gamma)
{
    /*
    double delta = sqrt((pow(x-p.x, 2) + pow(y-p.y, 2)));
    if ( delta < epsilon )
    {
        is_stopped = true;
    }
    */
    int new_x = gamma * (p.x - x) + x;
    int new_y = gamma * (p.y - y) + y;
    x = max(min(new_x, W-padding),1+padding);
    y = max(min(new_y, H-padding),1+padding);
    //x = new_x;
    //y = new_y;
}
